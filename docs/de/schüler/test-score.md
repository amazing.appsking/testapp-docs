# Testergebnisse

Hier siehst du das detaillierte Ergebnis eines Tests, nachdem dieser geschrieben wurde.

Oben befindet sich der gesamte Score des Tests, während sich darunter eine Liste der einzelnen Aufgaben befindet.

## Die einzelnen Aufgaben

Auf der linken Seite der Zeile ist ein Symbol. Der Haken bedeutet, du hast die Aufgabe richtig gelöst, während das Kreuz bedeutet, dass du eine falsche Antwort gegeben hast.

Wenn du auf eine Aufgabe drückst, siehst du genau, welche Antworten du gegeben hast und welche richtig sind.

!!! info
    Wenn du eine richtige Antwort gegeben hast, diese aber als falsch dargestellt wird, kontaktiere bitte deine Lehrer.

## Datenschutz

Wenn es sich bei dem Test um einen Join Test handelt, können Lehrer diesen über die [Kurs Score](../lehrer/kurs-score.md) Seite sehen.