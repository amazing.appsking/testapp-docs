# Das Dashboard

Das Dashboard der TestApp hat keine gro&szlig;artige eigene Funktion. Hier wird nur angezeigt, wie deine aktuellen Leistungen sind. Verlinkt sind hier [dein Score](score-schüler.md) sowie [Random Tests](random-test.md).