# Dein Score

Auf dieser Seite findest du eine Übersicht deiner Leistungen.

 - Dein gesamter Score
 - Dein Score pro Fach
 - Eine Liste deiner geschriebenen Tests

## Dein gesamter Score

Dein gesamter Score ist dein Durchschnitt aus dem letzten halben Jahr.

## Dein Score pro Fach

Hier findest du für alle deine Fächer deinen gesamt Score.

## Deine Tests

Der untere Teil der Seite enthält deine Tests. Sie sind nach dem Datum, an dem du sie geschrieben hast, sortiert.
 
!!! info
    Die Nummer am Anfang ist die ID deines Tests. Wenn du mit Lehrern über deinen Test reden möchtest, gibst du ihnen am Besten diese ID.

Wenn du auf die Zeile eines Tests drückst, wird dir der Score des Tests angezeigt. Wenn du mehr details sehen möchtest, wird dir der [Test Score](test-score.md) angezeigt.

Du kannst den Score eines Tests auch mit Freunden teilen.