# Als Schule registrieren

Prinzipiell kann jede Schule die TestApp enf&uuml;hren. Da das leider noch nicht automatisch geht, m&uuml;ssen wir bisher h&auml;ndisch unter [info@testapp.ga](mailto:info@testapp.ga) kontaktiert werden, damit interessierte Schulen einen Zugangscode erhalten.

Um die TestApp ermal anzuschauen, k&ouml;nnen sich einzelne Sch&uuml;ler und Lehrer aber gerne schon vorher unter `No School` bzw. `Keine Schule` anmelden. Bitte dazu die [Anleitung, wie man sich registriert](../allgemein/registrieren.md), lesen.

## Kostenfreie Testphase

Die TestApp ist 100% Open-Source. Das hei&szlig;t, dass alle Software, die wir verwenden und anbieten, Open-Source ist. Dennnoch ist f&uuml;r Schulen die Verwendung kostenpflichtig. Nachdem uns eine Schule kontaktiert hat, hat diese eine einj&auml;hrige Testzeit, um zu entscheiden, ob sie die TestApp verwenden will. Es gibt dabei keine Abo-Falle. Wenn die Zeit ohne R&uuml;ckmeldung abgelaufen ist, wird die Schule einfach wieder gel&ouml;scht.

## Preis

Den Preis kann man bei uns per Mail an die oben genannte Adresse erfragen. Wir mochten niemanden ausbeuten, sondern nur unsere Unkosten decken. Wir stellen uns den Preis in einem Bereich, wie etwa eine Schullekt&uuml;re pro Sch&uuml;ler vor