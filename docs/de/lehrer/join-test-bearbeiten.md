# Join Test bearbeiten

Hier können Join Tests bearbeitet und im Kurssatz geschrieben werden.

## Name des Join Tests

Hier kann der Name des Join Tests angegeben werden. Dieser wird für die Liste unter [Join Tests](join-tests.md) sowie im [Kurs Score](kurs-score.md#join-tests) verwendet und sollte auch für Kolleg\*innen verständlich sein.

## Aufgaben

Hier können die Aufgaben für den Join Test ausgewählt werden. Diese müssen die Schüler\*innen dann beim [Join Test schreiben](join-test-schreiben.md) bearbeiten. Dazu kannst du Aufgaben von der [Aufgaben](aufgaben.md)-Seite wählen.

## Test abzweigen

!!! info
    Das Folgende gillt nicht beim Erstellen eines Join Tests, sondern nur für bereits vorhandene Join Tests. Neue Join Tests bitte erst abspeichern und dann diese Seite aufrufen, um sie abzuzweigen, zu löschen oder zu schreiben.

Oben rechts findet sich das :fa-clone:-Symbol, mit dem der Join Test abgezweigt (oder *geforkt*) werden kann. Hierbei wird der Test mit den ausgewählten Aufgaben in einen neuen, leeren Test übertragen. Somit kann man kleine Änderungen an einem Join Test vornehmen, ohne die Aufgaben erneut heraussuchen zu müssen.

## Test löschen

Durch tippen auf das :fa-trash:-Symbol in der oberen, rechten Ecke kann der Join Test nach erneuter Bestätigung gelöscht werden.

## Join Test schreiben

Wie du einen Join Test im Kursverbund schreibst, erfährst du unter [Join Test schreiben](join-test-schreiben.md).