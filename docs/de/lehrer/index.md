# Die TestApp für Lehrer

## Weshalb sollte ich die TestApp verwenden

In ihrer Grundfunktion tut die TestApp nicht mehr, als ein analoger Test, den man mit Sch&uuml;ler\*innen schreibt. Allerdings gibt es neben dieser Grundfunktion noch einige Extras:

 - die Tests werden automatisch korrigiert
 - detaillierte Statistiken der Sch&uuml;lerleistungen nach Themengebiet
